import React from 'react'
import {Checkbox, Table} from 'semantic-ui-react'
import {Family} from "../api/generated/family";
import {CheckboxProps} from "semantic-ui-react/dist/commonjs/modules/Checkbox/Checkbox";

interface IFamiliesTable {
    families: Family[] | undefined;
    onFamilyCheck: (event: React.FormEvent<HTMLInputElement>, data: CheckboxProps) => void;
}

export default function FamiliesTable(familiesTable: IFamiliesTable) {

    let families = familiesTable.families;
    return (
        <Table sortable celled fixed compact selectable>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell/>
                    <Table.HeaderCell>
                        ID
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                        Фамилия
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {families && families.map(family => (
                        <Table.Row key={family.id}>
                            <Table.Cell>
                                <Checkbox value={family.id} onChange={familiesTable.onFamilyCheck}/>
                            </Table.Cell>
                            <Table.Cell>
                                {family.id}
                            </Table.Cell>
                            <Table.Cell>
                                {family.lastName}
                            </Table.Cell>
                        </Table.Row>
                    )
                )}
            </Table.Body>
        </Table>
    )
}