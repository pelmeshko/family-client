import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import MainMenu from "../MainMenu";
import {Child, Family, FamilyApi} from "../api/generated/family";
import {Container, Dimmer, Form, Header, Loader, Tab} from "semantic-ui-react";
import ChildTab from "./ChildTab";
import * as CommonAPI from "../api/Common";
import {ICommonClassifier} from "../api/Common";
import update from "immutability-helper";

interface IFamilyEdit {
    familyId: string
}


enum Status {
    loading,
    loaded
}

const api = new FamilyApi();
export default function FamilyEdit() {
    const {familyId} = useParams<IFamilyEdit>();
    const [family, updateFamily] = useState<Family>();
    const [classifiers, updateClassifiers] = useState<ICommonClassifier>({education: [], sex: []});
    const [status, setStatus] = useState<Status>(Status.loading);

    function onFamilyFetched(family: Family) {
        console.log(family);
        updateFamily(family)


    }

    function handleSubmit() {
        console.log("family", family);
    }

    useEffect(() => {
        Promise.all([api.familyFamilyIDGET(familyId), CommonAPI.getClassifiers()])
            .then(([family, classifiers]) => {
                onFamilyFetched(family.data);
                updateClassifiers(classifiers.data);
                setStatus(Status.loaded);
            })
    }, [familyId]);

    function onChildStateChanged(child: Child) {
        console.log("onChildDataChanged", child);

        updateFamily(prevState => {
            if (!prevState?.children) return;
            const childIndex = prevState?.children.findIndex(function (c) {
                return c.name === child.name;
            });
            const children = update(prevState.children, {$splice: [[childIndex, 1, child]]})
            const result = {...prevState, children};
            console.log("mergeResult:", children)
            return result;
        });
    }

    let childrenTabs;
    if (family) {
        childrenTabs = family.children?.map(child => {
            return {
                menuItem: child.name,
                render: () => (<ChildTab child={child} classifiers={classifiers} onStateChanged={onChildStateChanged}/>)
            }
        });
    }

    let form;
    if (status.valueOf() === Status.loading.valueOf()) {
        form = <Container>
            <MainMenu/>
            <Header as='h1'>Семья [{family?.id}]</Header>
            <Dimmer active inverted>
                <Loader inverted>Загрузка...</Loader>
            </Dimmer>
        </Container>
    }else {
        form = <Container>
            <MainMenu/>
            <Header as='h1'>Семья [{family?.id}]</Header>
            <Form onSubmit={handleSubmit}>
                <Tab panes={childrenTabs} />
            </Form>
        </Container>
    }
    return form
}