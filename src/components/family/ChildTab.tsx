import React from 'react'
import {Form, Tab} from 'semantic-ui-react'
import {Child} from "../api/generated/family";
import {ICommonClassifier} from "../api/Common";

interface IChildTab {
    child: Child,
    classifiers: ICommonClassifier,
    onStateChanged: (child: Child) => void
}

export default function ChildTab(props: IChildTab) {
    const educationSelectData = props.classifiers.education.map(({code, text}) => (
        {key: code, text: text, value: code})
    );

    function changed(event: any, data: any) {
        props.onStateChanged({...props.child, [data.name]: data.value});
    }

    return (
        <Tab.Pane key={props.child.name}>
            <Form.Input fluid label='Образование' name='education'
                        value={props.child.education} onChange={changed}/>
            <Form.Select fluid label='Образование Select' name='educationSelect'
                         value={props.child.education.toString()}
                         options={educationSelectData} onChange={changed}/>

        </Tab.Pane>
    )
}