import React, {useEffect, useState} from "react";
import {Family} from "../api/generated/family";
import {FamilyApi} from "../api/generated/family";
import {AxiosResponse} from "axios";
import MainMenu from "../MainMenu";
import {Container, Dropdown} from "semantic-ui-react";
import {NavLink, useHistory} from "react-router-dom";
import FamiliesTable from "./FamiliesTable";
import {CheckboxProps} from "semantic-ui-react/dist/commonjs/modules/Checkbox/Checkbox";

const api = new FamilyApi();

export default function FamilyFilter() {
    const [families, updateFamilyList] = useState<Family[]>();
    const [selectedFamilies, updateSelectedItems] = useState<{familyID:string}[]>([]);
    const history = useHistory();

    function onFamiliesLoaded(response: AxiosResponse<Family[]>) {
        console.log(response.data);
        updateFamilyList(response.data);
    }

    useEffect(() => {
        api.searchFamily().then(onFamiliesLoaded);
    }, []);

    function onFamilyCheck(event: React.FormEvent<HTMLInputElement>, data: CheckboxProps){
        if (typeof data.value === "string") {
            const d = data.value;
            if (data.checked) {
                updateSelectedItems(prevState => (prevState.concat([{familyID: d}])))
            } else {
                updateSelectedItems(prevState => prevState.filter((family => {
                    return data.value !== family.familyID
                })))
            }
        }
    }

    function onEditClick(){
        if (selectedFamilies.length>0){
            history.push("/family/"+selectedFamilies[0].familyID);
        }
    }

    return (
        <Container>
            <MainMenu>
                <Dropdown text='Действия' pointing className='link item'>
                    <Dropdown.Menu>
                        <Dropdown.Item text='Добавить' as={NavLink} to="/family/0"/>
                        <Dropdown.Item text='Редактировать' onClick={onEditClick}/>
                    </Dropdown.Menu>
                </Dropdown>
            </MainMenu>
            <div>Фильтр</div>
            <FamiliesTable families={families} onFamilyCheck={onFamilyCheck}/>
        </Container>
    )
}