import React from 'react'
import {Menu} from 'semantic-ui-react'
import {NavLink} from "react-router-dom";

export default function MainMenu(props: any) {
    return (
        <Menu>
            <Menu.Item exact as={NavLink} to="/" name='index'>Главная</Menu.Item>
            {props.children}
        </Menu>
    )
}