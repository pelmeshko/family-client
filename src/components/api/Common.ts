import {BaseClassifier} from "../../models/Classifiers";

export interface ICommonClassifier {
    education: BaseClassifier[],
    sex: BaseClassifier[]
}

export function getClassifiers() {
    let promise: Promise<{ data: ICommonClassifier }>;
    promise = new Promise(resolve => {
        setTimeout(() => {
            resolve({
                data: {
                    education:
                        [
                            {code: "1", text: "высшее"},
                            {code: "2", text: "среднее"},
                            {code: "3", text: "ниже среднего"}
                        ],
                    sex:
                        [
                            {code: "f", text: "жен."},
                            {code: "m", text: "муж."}
                        ]
                }
            })
        }, 1000);
    });
    return promise;
}