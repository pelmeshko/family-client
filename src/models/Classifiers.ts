class BaseClassifier {
    code: string;
    text: string;

    constructor(code: string, value: string) {
        this.code = code;
        this.text = value;
    }
}

export {BaseClassifier}
