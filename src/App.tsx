import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import {BrowserRouter as Router, Route} from "react-router-dom"
import FamilyFilter from "./components/family/FamilyFilter";
import FamilyEdit from "./components/family/FamilyEdit";

function App() {
  return (
    <div className="App">
      <Router>
        <Route
            path="/"
            exact
            render={() => <FamilyFilter/>}/>
        <Route
            path="/family/:familyId"
            render={() => <FamilyEdit/>}/>
      </Router>
    </div>
  );
}

export default App;
